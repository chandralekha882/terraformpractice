module "ec2" {
  source = "./ec2"
  Name   = var.Name
}

output "EC2_PUBLIC_IP" {
  value = module.ec2.EC2_PUBLIC_IP
}